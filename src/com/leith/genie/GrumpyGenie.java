package com.leith.genie;

public class GrumpyGenie extends Genie {
    public GrumpyGenie(int maxNumOfWishes) {
        super(maxNumOfWishes);
        System.out.println("Grumpy genie spawned!");
    }

    @Override
    public void grantWish() {
        if (super.getRemainingNumOfWishes() > 0) {
            super.grantWish();
            super.setRemainingNumOfWishes(0);
        } else {
            System.out.println("I'm grumpy and I gave you a wish. Leave me alone.");
        }
    }
}
