package com.leith.genie;

public class DemonGenie extends Genie {
    private boolean isRecycled;
    public DemonGenie(int maxNumOfWishes) {
        super(maxNumOfWishes);
        isRecycled = false;
        System.out.println("Demon spawned!");
    }

    public void setRecycled() {
        isRecycled = true;
    }

    public boolean isRecycled() {
        return isRecycled;
    }

    @Override
    public void grantWish() {
        if (!isRecycled) {
            super.grantWish();
        } else {
            System.out.println("I can't grant any more wishes.");
        }
    }
}
