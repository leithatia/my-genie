package com.leith.genie;

import java.util.Random;

public class MagicLamp {
    private final int maxNumOfGenies;
    private int remainingNumOfGenies;
    private int timesRecharged;

    public MagicLamp(int maxNumOfGenies) {
        this.maxNumOfGenies = maxNumOfGenies;
        remainingNumOfGenies = maxNumOfGenies;
        timesRecharged = 0;
    }

    public Genie rubLamp() {
        if (remainingNumOfGenies > 0) {
            if (remainingNumOfGenies-- % 2 == 0) {
                return new FriendlyGenie(randNumFromRange(3, 6));
            } else {
                return new GrumpyGenie(randNumFromRange(2, 4));
            }
        }
        return new DemonGenie(randNumFromRange(6, 10));
    }

    public void recycle(Genie genie) {
        DemonGenie demon = (DemonGenie) genie;

        if (demon.isRecycled()) {
            System.out.println("Demon has already been recycled.");
            return;
        }

        demon.setRecycled();
        System.out.println("Lamp has been recharged!");
        remainingNumOfGenies = maxNumOfGenies;
        timesRecharged++;
    }

    public static void compareLamps(MagicLamp lamp1, MagicLamp lamp2) {
        System.out.println("Lamp 1:");
        System.out.println(lamp1);
        System.out.println("Lamp 2:");
        System.out.println(lamp2);
    }

    private int randNumFromRange(int min, int max) {
        return RandomGenerator.randomNumFromRange(min, max);
    }

    @Override
    public String toString() {
        return "This lamp has " + remainingNumOfGenies + " out of " + maxNumOfGenies + " genies left and has been recharged "
                + timesRecharged + " times.\n";
    }
}
