package com.leith.genie;

import java.lang.module.FindException;

public class Main {
    public static void main(String[] args) {
        MagicLamp lamp1 = new MagicLamp(2);
        MagicLamp lamp2 = new MagicLamp(3);

        System.out.println(lamp1);

        Genie genie1 = lamp1.rubLamp();
        System.out.println(lamp1);
        Genie genie2 = lamp1.rubLamp();
        System.out.println(lamp1);
        Genie genie3 = lamp1.rubLamp();
        System.out.println(lamp1);

        genie1.grantWish();
        genie1.grantWish();
        genie1.grantWish();
        genie1.grantWish();

        genie2.grantWish();
        genie2.grantWish();

        genie3.grantWish();
        genie3.grantWish();
        genie3.grantWish();
        genie3.grantWish();
        genie3.grantWish();

        System.out.println(lamp1);

        lamp1.recycle(genie3);
        lamp1.recycle(genie3);

        genie3.grantWish();

        Genie genie4 = lamp1.rubLamp();

        System.out.println(lamp1);


        Genie genie5 = lamp1.rubLamp();

        System.out.println(lamp1);

        MagicLamp.compareLamps(lamp1, lamp2);
    }
}
