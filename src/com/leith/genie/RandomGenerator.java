package com.leith.genie;

public class RandomGenerator {
    public static int randomNumFromRange(int min, int max) {
        int range = max - min + 1;
        return (int) (Math.random() * range ) + min;
    }
}
