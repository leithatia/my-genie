package com.leith.genie;

public class Genie {
    private int remainingNumOfWishes;

    public Genie(int maxNumOfWishes) {
        remainingNumOfWishes = maxNumOfWishes;
    }

    public void setRemainingNumOfWishes(int remainingNumOfWishes) {
        this.remainingNumOfWishes = remainingNumOfWishes;
    }

    public int getRemainingNumOfWishes() {
        return remainingNumOfWishes;
    }

    public void grantWish() {
        System.out.println("Your wish was granted. You have " + remainingNumOfWishes + " wishes left. Alabaallbah!");
        if (remainingNumOfWishes > 0) {
            remainingNumOfWishes--;
        }
    }
}
