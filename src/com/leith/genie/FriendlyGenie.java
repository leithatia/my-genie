package com.leith.genie;

public class FriendlyGenie extends Genie {
    public FriendlyGenie(int maxNumOfWishes) {
        super(maxNumOfWishes);
        System.out.println("Friendly genie spawned!");
    }

    @Override
    public void grantWish() {
        if (super.getRemainingNumOfWishes() > 0) {
            super.grantWish();
        } else {
            System.out.println("No more wishes for you.");
        }
    }
}
